from django.contrib import admin
from todos.models import (
    TodoList,
    TodoItem,
)

# Register your models here.


class TodoListAdmin(admin.ModelAdmin):
    pass


class TodoItemAdmin(admin.ModelAdmin):
    pass


admin.site.register(TodoItem, TodoItemAdmin)
admin.site.register(TodoList, TodoListAdmin)
