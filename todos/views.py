from django.shortcuts import render
from django.views.generic.list import ListView
from todos.models import TodoList
from django.views.generic.detail import DetailView

# Create your views here.


class TodoListView(ListView):
    model = TodoList
    context_object_name = "todolist"
    template_name = "todo/todolist_list.html"

    # def get_context_data(self, *args, **kwargs):
    #     context=super().get_context_data(*args, **kwargs)
    #     print(context)
    #     return context


class TodoListDetailView(DetailView):
    model = TodoList
    template_name = "todos/todolist_detail.html"
    context_object_name = "todoitem"

    # def get_context_data(self, *args, **kwargs):
    #     context=super().get_context_data(*args, **kwargs)
    #     print(context)
    #     return context
